﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Balance.Models
{
    public class DataFront
    {
        public int IDFila { get; set; }
        public string Patio { get; set; }
        public decimal FlujoRiego { get; set; }
        public decimal FlujoPls { get; set; }
        public decimal LeyCu { get; set; }
        public decimal Toneladas { get; set; }
        public string Concepto { get; set; }
        public decimal Valor { get; set; }
    }
}
