﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Balance.Models
{
    public class DataFrontConfig
    {
        public int IDFila { get; set; }
        public string Patio { get; set; }
        public int FlujoRiego { get; set; }
        public int FlujoPls { get; set; }
        public int LeyCu { get; set; }
        public int Tonelaje { get; set; }
    }
}
