﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace API_Balance.Models
{
    public class DatosBalance
    {
        [Key]
        public int ID { get; set; }
        public int IDFilaPlantilla { get; set; }
        public string Patio { get; set; }
        public decimal FlujoRiego { get; set; }
        public decimal FlujoPLS { get; set; }
        public decimal LeyCu { get; set; }
        public decimal Tonelaje { get; set; }
        public DateTime FechaReal { get; set; }
        public DateTime FechaInsert { get; set; }

    }
}