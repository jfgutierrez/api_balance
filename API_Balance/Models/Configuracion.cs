﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_Balance.Models
{
    public class Configuracion
    {
        [Key]
        public int ID { get; set; }
        public int IDFilaPlantilla { get; set; }
        public int FlujoRiego_ID { get; set; }
        public int FlujoPLS_ID { get; set; }
        public int LeyCu_ID { get; set; }
        public int Tonelaje_ID { get; set; }

    }
}
