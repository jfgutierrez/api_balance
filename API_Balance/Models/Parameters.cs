﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Balance.Models
{
    public class Parameters
    {
        public List<DataFront> dataFront { get; set; }
        public string fechaFront { get; set; }
        public string userName { get; set; }
    }
}
