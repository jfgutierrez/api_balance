﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace API_Balance.Models
{
    public class DatosBalanceParamControl
    {
        [Key]
        public int ID { get; set; }
        public int IDFilaPlantilla { get; set; }
        public string Concepto { get; set; }
        public decimal Valor { get; set; }
        public DateTime FechaReal { get; set; }
        public DateTime FechaInsert { get; set; }
    }
}
