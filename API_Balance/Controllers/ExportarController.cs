﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using API_Balance.Context;
using API_Balance.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.Data;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace API_Balance.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExportarController : ControllerBase
    {
        private readonly DataContext context;
        
        public ExportarController(DataContext context)
        {
            this.context = context;
        }

        [HttpGet("config", Name ="getConfig")]
        public ActionResult GetConfig()
        {
            try
            {
                return Ok(context.ConfigVariables.ToList());
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }
            // POST: ExportarController/Create
            [HttpPost("savedata")]
        public ActionResult SaveData([FromBody] Parameters parameters)
        {
            try
            {
                DateTime fecha = new DateTime();
                fecha = DateTime.Now;

                var fachafront = DateTime.Parse(parameters.fechaFront);
                var usuario = parameters.userName;
                var datos = parameters.dataFront.ConvertAll(dt => new DatosBalance()
                {
                    IDFilaPlantilla = dt.IDFila,
                    Patio = dt.Patio,
                    FlujoRiego = dt.FlujoRiego,
                    FlujoPLS = dt.FlujoPls,
                    LeyCu = dt.LeyCu,
                    Tonelaje = dt.Toneladas,
                    FechaReal = fachafront,
                    FechaInsert = fecha
                });

                context.DatosBalance.AddRange(datos);
                context.SaveChanges();

                var fechaParam = fachafront.Date.ToString("yyyyMMdd");

                using (var commad = context.Database.GetDbConnection().CreateCommand())
                {
                    commad.CommandText = "spDatos_Balance_Metalurgico";
                    commad.CommandType = CommandType.StoredProcedure;
                    commad.Parameters.Add( new SqlParameter("@fecha", fechaParam));
                    commad.Parameters.Add(new SqlParameter("@accion", 1));
                    commad.Parameters.Add(new SqlParameter("@userName", usuario));
                    context.Database.OpenConnection();
                    commad.ExecuteNonQuery();
                    context.Database.CloseConnection();
                }

                var datosContol = parameters.dataFront.ConvertAll(dtc => new DatosBalanceParamControl()
                {
                    IDFilaPlantilla = dtc.IDFila,
                    Concepto = dtc.Concepto != null ? dtc.Concepto: "N/A",
                    Valor = dtc.Valor != 0 ? dtc.Valor: 0,
                    FechaReal = fachafront,
                    FechaInsert = fecha
                });

                context.DatosBalanceParamControls.AddRange(datosContol);
                context.SaveChanges();

                using (var commad = context.Database.GetDbConnection().CreateCommand())
                {
                    commad.CommandText = "spDatos_Balance_Metalurgico";
                    commad.CommandType = CommandType.StoredProcedure;
                    commad.Parameters.Add(new SqlParameter("@fecha", fechaParam));
                    commad.Parameters.Add(new SqlParameter("@accion", 2));
                    commad.Parameters.Add(new SqlParameter("@userName", usuario));
                    context.Database.OpenConnection();
                    commad.ExecuteNonQuery();
                    context.Database.CloseConnection();
                }

                return Ok(parameters);
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpGet("getvariables")]
        public ActionResult GetVariables()
        {
            try
            {
                //var variables = context.Variables.ToList();

                var variables = context.Variables.FromSqlRaw("SELECT v.id, v.texto as Descripcion FROM Variables as v JOIN Formularios_Rel_Variables as frv on v.id = frv.IdVariable WHERE frv.IdFormulario =  1111 ORDER BY frv.orden asc").ToList();
                return Ok(variables);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("saveconfig")]
        public ActionResult SaveConfig([FromBody] DataFrontConfig dataFrontConfig)
        {
            try
            {
                Configuracion config = new Configuracion()
                {
                    IDFilaPlantilla = dataFrontConfig.IDFila,
                    FlujoRiego_ID = dataFrontConfig.FlujoRiego,
                    FlujoPLS_ID = dataFrontConfig.FlujoPls,
                    LeyCu_ID = dataFrontConfig.LeyCu,
                    Tonelaje_ID = dataFrontConfig.Tonelaje
                };

                context.ConfigVariables.Add(config);
                context.SaveChanges();

                return Ok(context.ConfigVariables.ToList());

            }
            catch (Exception e)
            {

                return BadRequest(e.Message);
            }
        }

        //// GET: ExportarController/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        //// POST: ExportarController/Edit/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit(int id, IFormCollection collection)
        //{
        //    try
        //    {
        //        return RedirectToAction(nameof(Index));
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: ExportarController/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: ExportarController/Delete/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Delete(int id, IFormCollection collection)
        //{
        //    try
        //    {
        //        return RedirectToAction(nameof(Index));
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
