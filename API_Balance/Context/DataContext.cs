﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_Balance.Models;
using Microsoft.EntityFrameworkCore;

namespace API_Balance.Context
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options): base(options)
        {
        }

        public DbSet<DatosBalance> DatosBalance { get; set; }
        public DbSet<Configuracion> ConfigVariables { get; set; }
        public DbSet<Variable> Variables { get; set; }
        public DbSet<DatosBalanceParamControl> DatosBalanceParamControls { get; set; }
    }
}
